﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BestRaceTime.Models
{
    public class RaceTime
    {
        public int Id { get; set; }

        [Required]
        [StringLength(15)]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(20)]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Required]
        [RegularExpression(@"([0, 1][0 - 9])|(2[0-4])(:([0 - 5][0 - 9])){2}")]
        [Display(Name = "Time")]
        public string Time { get; set; }

        public bool Approved { get; set; }
    }
}
