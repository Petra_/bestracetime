﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BestRaceTime.Models.DataSet
{
    public static class DbInitializer
    {
        public static async Task Seed(AppDbContext context, UserManager<IdentityUser> _userManager, RoleManager<IdentityRole> _roleManager)
        {
            if (!context.Users.Any())
            {
                IdentityUser identityUserAdmin = new IdentityUser()
                {
                    UserName = "admin@admin.admin",
                    Email = "admin@admin.admin"
                };
                var result = await _userManager.CreateAsync(identityUserAdmin);
                if (result.Succeeded)
                {
                    await _userManager.AddPasswordAsync(identityUserAdmin, "Admin123!");
                }

                await _roleManager.CreateAsync(new IdentityRole("Admin"));
                await _userManager.AddToRoleAsync(identityUserAdmin, "Admin");


                IdentityUser identityUser = new IdentityUser()
                {
                    UserName = "user@user.user",
                    Email = "user@user.user"
                };
                var result1 = await _userManager.CreateAsync(identityUser);
                if (result1.Succeeded)
                {
                    await _userManager.AddPasswordAsync(identityUser, "User123!");
                }

                context.SaveChanges();
            }

            if (!context.RaceTimes.Any())
            {
                context.AddRange
                (
                    new RaceTime { FirstName = "Ana", LastName = "Anić", Time = "01:01:01", Approved = true},
                    new RaceTime { FirstName = "Iva", LastName = "Ivić", Time = "01:54:12", Approved = true },
                    new RaceTime { FirstName = "Marko", LastName = "Makrić", Time = "00:14:48", Approved = true },
                    new RaceTime { FirstName = "Dean", LastName = "Deanović", Time = "02:03:04", Approved = true },
                    new RaceTime { FirstName = "Maja", LastName = "Majić", Time = "01:36:04", Approved = true },
                    new RaceTime { FirstName = "Branka", LastName = "Brankić", Time = "00:56:14", Approved = false },
                    new RaceTime { FirstName = "Ema", LastName = "Emić", Time = "01:15:21", Approved = false },
                    new RaceTime { FirstName = "Jakov", LastName = "Jakić", Time = "01:41:54", Approved = false },
                    new RaceTime { FirstName = "Nina", LastName = "Ninić", Time = "01:27:31", Approved = false }
                );

                context.SaveChanges();
            }
        }
    }
}
