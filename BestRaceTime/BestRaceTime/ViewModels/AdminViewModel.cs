﻿using BestRaceTime.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BestRaceTime.ViewModels
{
    public class AdminViewModel
    {
        public List<RaceTime> RaceTimesToApprove { get; set; }
        public List<RaceTime> ApprovedRaceTimes { get; set; }
    }
}
