﻿using BestRaceTime.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BestRaceTime.ViewModels
{
    public class RaceTimeViewModel
    {
        public RaceTime NewRaceTime { get; set; }
        public List<RaceTime> RaceTimes { get; set; }
    }
}
