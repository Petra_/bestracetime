﻿
$(document).ready(function () {
    $("#NewRaceTime_Time").keyup(function (e) {
        ValidateRaceTime();
    });
    $("#NewRaceTime_Time").change(function (e) {
        ValidateRaceTime();
    });

    $("#submit").prop('disabled', true);
    $("#dateError").hide();

    $("#newRaceTimeForm").on("submit", function (e) {
        submitNewRaceTimeForm();
        e.preventDefault();
    });
});

function ValidateRaceTime() {
    //console.log("ValidateRaceTime");
    var timeString = $("#NewRaceTime_Time").val();
    var regex = /([0,1][0-9])|(2[0-4])(:([0-5][0-9])){2}$/;
    if (regex.test(timeString)) {
        $("#dateError").hide();
        $("#submit").prop('disabled', false);
    } else {
        $("#dateError").show();
        $("#submit").prop('disabled', true);
    }
}

function submitNewRaceTimeForm() {
    var param = {
        firstName: $("#NewRaceTime_FirstName").val(),
        lastName: $("#NewRaceTime_LastName").val(),
        time: $("#NewRaceTime_Time").val(),
    };

    $.ajax({
        type: "POST",
        url: "/Home/NewRaceTime",
        dataType: "json",
        data: param,
        success: function (data) {
            //console.log("success - submitCalculatorForm");
            if (data) {
                alert("Your race time has forwarded for approval.");
            } else {
                alert("Invalid data");
            }
        },
        failure: function (data) {
            alert(data.responseText);
            //console.log("failure");
        }, //End of AJAX failure function
        error: function (data) {
            //debugger
            alert(data.responseText);
            //console.log("error");
        } //End of AJAX error function
    });
}