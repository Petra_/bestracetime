﻿$(document).ready(function () {
    $(".approve").on("click", function (e) {
        ApproveRaceTime(e.target.id);
    });
    $(".delete").on("click", function (e) {
        DeleteRaceTime(e.target.id);
    });
});

function ApproveRaceTime(raceTimeId) {
    window.location.href = "/Admin/Approve/" + raceTimeId;
}

function DeleteRaceTime(raceTimeId) {
    var result = confirm("Are you sure you want to delete this race time?");
    if (result == true) {
        window.location.href = "/Admin/Delete/" + raceTimeId;
    }
}