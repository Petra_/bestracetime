﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BestRaceTime.Interfaces;
using BestRaceTime.Models;
using BestRaceTime.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace BestRaceTime.Controllers
{
    public class HomeController : Controller
    {
        private readonly IRaceTimeRepository _raceTimeRepository;

        public HomeController(IRaceTimeRepository raceTimeRepository)
        {
            _raceTimeRepository = raceTimeRepository;
        }

        public IActionResult Index()
        {
            RaceTimeViewModel raceTimeViewModel = new RaceTimeViewModel
            {
                RaceTimes = _raceTimeRepository.GetBestRaceTimes()
            };

            return View(raceTimeViewModel);
        }

        public JsonResult NewRaceTime(string firstName, string lastName, string time)
        {
            if(!ModelState.IsValid) return Json(false);

            RaceTime raceTime = new RaceTime
                {
                    FirstName = firstName,
                    LastName = lastName,
                    Time = time,
                    Approved = false
                };
            _raceTimeRepository.AddRaceTime(raceTime); 
            
            return Json(true); 
        }
    }
}