﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BestRaceTime.Interfaces;
using BestRaceTime.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace BestRaceTime.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private readonly IRaceTimeRepository _raceTimeRepository;
        private readonly UserManager<IdentityUser> _userManager;

        public AdminController(IRaceTimeRepository raceTimeRepository, UserManager<IdentityUser> userManager)
        {
            _raceTimeRepository = raceTimeRepository;
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            AdminViewModel adminViewModel = new AdminViewModel
            {
                RaceTimesToApprove = _raceTimeRepository.GetRaceTimesToApprove(),
                ApprovedRaceTimes = _raceTimeRepository.GetApprovedRaceTimes()
            };

            return View(adminViewModel);
        }

        [Route("Admin/Approve/{raceTimeId}")]
        public async Task<IActionResult> Approve(int raceTimeId)
        {
            var applicationUser = await _userManager.GetUserAsync(HttpContext.User);
            var userRoles = await _userManager.GetRolesAsync(applicationUser);
            
            if (userRoles.FirstOrDefault() == "Admin")
            {
                _raceTimeRepository.Approve(raceTimeId);
            }

            return RedirectToAction("Index", "Admin");
        }


        [Route("Admin/Delete/{raceTimeId}")]
        public async Task<IActionResult> Delete(int raceTimeId)
        {
            var applicationUser = await _userManager.GetUserAsync(HttpContext.User);
            var userRoles = await _userManager.GetRolesAsync(applicationUser);

            if (userRoles.FirstOrDefault() == "Admin")
            {
                _raceTimeRepository.Delete(raceTimeId);
            }

            return RedirectToAction("Index", "Admin");
        }


    }
}