﻿using BestRaceTime.Interfaces;
using BestRaceTime.Models;
using BestRaceTime.Models.DataSet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BestRaceTime.Repositories
{
    public class RaceTimeRepository : IRaceTimeRepository
    {
        private readonly AppDbContext _appDbContext;

        public RaceTimeRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public void AddRaceTime(RaceTime raceTime)
        {
            _appDbContext.RaceTimes.Add(raceTime);
            _appDbContext.SaveChanges();
        }

        public void Approve(int raceTimeId)
        {
            var raceTime = _appDbContext.RaceTimes
                .Where(time => time.Id == raceTimeId)
                .FirstOrDefault();
            if (raceTime != null)
            {
                raceTime.Approved = true;
                _appDbContext.SaveChanges();
            }
        }

        public void Delete(int raceTimeId)
        {
            var raceTime = _appDbContext.RaceTimes
                .Where(time => time.Id == raceTimeId)
                .FirstOrDefault();
            if (raceTime != null)
            {
                _appDbContext.RaceTimes.Remove(raceTime);
                _appDbContext.SaveChanges();
            }
        }

        public List<RaceTime> GetApprovedRaceTimes()
        {
            return _appDbContext.RaceTimes
                .Where(time => time.Approved)
                .OrderBy(time => time.Time)
                .ToList();
        }

        public List<RaceTime> GetBestRaceTimes(int numOfRecords = 15)
        {
            return _appDbContext.RaceTimes
                .Where(time => time.Approved)
                .OrderBy(time => time.Time)
                .Take(numOfRecords)
                .ToList();
        }

        public List<RaceTime> GetRaceTimesToApprove()
        {
            return _appDbContext.RaceTimes
                .Where(time => !time.Approved)
                .ToList();
        }

    }
}
