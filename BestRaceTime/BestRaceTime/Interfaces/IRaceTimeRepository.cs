﻿using BestRaceTime.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BestRaceTime.Interfaces
{
    public interface IRaceTimeRepository
    {
        void AddRaceTime(RaceTime raceTime);
        List<RaceTime> GetBestRaceTimes(int numOfRecords = 15);
        List<RaceTime> GetRaceTimesToApprove();
        List<RaceTime> GetApprovedRaceTimes();
        void Approve(int raceTimeId);
        void Delete(int raceTimeId);
    }
}
